// TODO 20: 在这里写实现代码
import Person from './person';

class Teacher extends Person {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }

  introduce() {
    let klassNumber;
    if (this.klass == null) {
      klassNumber = 'No Class';
    } else {
      klassNumber = `Class ${this.klass}`;
    }
    return `${super.introduce()} I am a Teacher. I teach ${klassNumber}.`;
  }
}

export default Teacher;
