function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  return collection.filter(item => item % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  const noRepeat = collection.filter((item, index, array) => array.indexOf(item) === index);
  return noRepeat;
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
