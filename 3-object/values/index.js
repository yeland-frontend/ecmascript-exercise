export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  return Object.values(source).reduce((pre, ele) => parseInt(pre, 10) + parseInt(ele, 10));
}
