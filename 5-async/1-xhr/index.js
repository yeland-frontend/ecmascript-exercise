function fetchData(url, successCallback, errorCallback) {
  const xhr = new XMLHttpRequest();
  // <-- start
  // TODO 21: 通过XMLHttpRequest实现异步请求
  xhr.onload = () => successCallback(xhr.responseText);
  xhr.onerror = () => errorCallback(new Error(xhr.statusText));
  xhr.open('get', url, true);
  xhr.send(null);
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(
  URL,
  result => {
    document.writeln(JSON.parse(result).name);
  },
  error => {
    console.error(error);
  }
);
